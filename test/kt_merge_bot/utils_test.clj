(ns kt-merge-bot.utils-test
  (:require [clojure.test :refer :all]
            [kt-merge-bot.utils :as u]))

(deftest retry-test-simple
  (testing "retry returns success on success"
    (is (= "abc" (u/retry {:catcher (fn [_] true) } "abc") )))
  (testing "retry fails on failure"
    (is (thrown? Exception (u/retry {:catcher (fn [_] true) } (throw (Exception.))) ))))

(deftest retry-test-attempts
  (testing "retry several times"
    (let [i (atom 0)]
      (is (thrown? Exception (u/retry {:catcher (fn [_] true) :repeats 4}
                                      (swap! i inc)
                                      (throw (Exception.))) ))
      (is (= 5 @i))))
  (testing "retry one time because of non-catched exception"
    (let [i (atom 0)]
      (is (thrown? Exception (u/retry {:catcher (fn [_] false) :repeats 4}
                                      (swap! i inc)
                                      (throw (Exception.))) ))
      (is (= 1 @i))))
  (testing "retry several times and not throw"
    (let [i (atom 0)]
      (is (= nil (u/retry {:catcher (fn [_] true) :repeats 4 :not-throw? true}
                                      (swap! i inc)
                                      (throw (Exception.))) ))
      (is (= 5 @i)))))
