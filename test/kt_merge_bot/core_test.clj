(ns kt-merge-bot.core-test
  (:require [clojure.test :refer :all]
            [kt-merge-bot.parser :refer :all]
            [kt-merge-bot.git-info :refer :all]
            [kt-merge-bot.test-utils :refer :all]
            [kt-merge-bot.utils :refer :all]
            [kt-merge-bot.core :refer :all]
            [clojure.string :as str])
  (:import (java.time Instant)
           (java.util Date)
           (org.jetbrains.teamcity.rest BuildId TeamCityConversationException)))

(deftest get-conflict-files0test
  (testing "parsing-merge-result"
    (is (= (list "versions.gradle.kts"
             "ultimate/src/org/jetbrains/kotlin/idea/nodejs/protractor/KotlinProtractorRunConfigurationProducer.kt"
             "ultimate/src/org/jetbrains/kotlin/idea/nodejs/mocha/KotlinMochaRunConfigurationProducer.kt"
             "prepare/idea-plugin/build.gradle.kts"
             "plugins/android-extensions/android-extensions-idea/build.gradle.kts"
             "idea/src/org/jetbrains/kotlin/idea/k2jsrun/K2JSRunConfiguration.java"
             "idea/src/org/jetbrains/kotlin/idea/js/jsUtils.kt"
             "idea/idea-gradle/src/org/jetbrains/kotlin/idea/configuration/KotlinGradleSourceSetDataService.kt"
             "idea/idea-gradle/build.gradle.kts"
             "generators/src/org/jetbrains/kotlin/generators/tests/GenerateTests.kt") (get-conflict-files (str/split (slurp "testData/conflict-modify-delete.txt") #"\n"))))))


(deftest get-compile-errors-from-latest-build-test
  (testing "get-compile-errors-from-latest-build"
    (with-redefs [kt-merge-bot.teamcity/latest-build-for-configuration
                  (sequential-mock [{:args ["master" "Kotlin_dev_CompilerAndPlugin_172"]}
                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_as30"] :fail true}
                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_as30"]
                                       :out  (reify org.jetbrains.teamcity.rest.Build
                                                  (fetchStatusText [this] "compile error")
                                                  (fetchFinishDate [this] (Date/from (Instant/ofEpochSecond 1517606145)))
                                                  (getId [this] (BuildId. "myId"))
                                                  (getWebUrl [this] "http://build.url"))}
                                      {:args ["1.2.50" "Kotlin_dev_CompilerAndPlugin_1"]}]
                                   (fn [line] (or (:out line) (when (:fail line) (throw (TeamCityConversationException. "test error" nil))))))
                  kt-merge-bot.teamcity/get-compilation-errors
                  (fn [param] (is (= param "myId"))
                    [{:file "compiler/cli/src/org/jetbrains/kotlin/cli/jvm/compiler/KotlinCoreEnvironment.kt",
                      :msg  "(31, 26): Unresolved reference: jvm"}
                     {:file "compiler/cli/src/org/jetbrains/kotlin/cli/AnotherFile.kt",
                      :msg  "anothererror"}])]
      (expect-proc-calls [{:args ["git" "checkout" "-f" "origin/master"] :out "master\n"}
                          {:args ["git" "log" "-n" "1" "--format=%H %ct %ae %ce %s" "--" "compiler/cli/src/org/jetbrains/kotlin/cli/jvm/compiler/KotlinCoreEnvironment.kt"] :out "f05e9266705b7dfc64cedd5eedc66605e6ddcb64 1517606145 nikolay.krasko@jetbrains.com nikolay.krasko@jetbrains.com Update idea to 173.3.4 (173.4548.28)\n"}
                          {:args ["git" "log" "-n" "1" "--format=%H %ct %ae %ce %s" "--" "compiler/cli/src/org/jetbrains/kotlin/cli/AnotherFile.kt"] :out "f05e9266705b7dfc64cedd5eedc66605e6ddcb64 1517606145 nikolay.krasko@jetbrains.com nikolay.krasko@jetbrains.com Update idea to 173.3.4 (173.4548.28)\n"}
                          {:args ["git" "checkout" "-f" "origin/1.2.50"] :out "1.2.50\n"}
                          ]
                         (is (= (list {:bunch "master_172"
                                       :branch   "master"}
                                      {:build-url   "http://build.url"
                                       :conflicts   [{:commit {:hash    "f05e9266705b7dfc64cedd5eedc66605e6ddcb64"
                                                               :mail    "nikolay.krasko@jetbrains.com"
                                                               :message "Update idea to 173.3.4 (173.4548.28)"
                                                               :time    (Instant/ofEpochSecond 1517606145)}
                                                      :file   "compiler/cli/src/org/jetbrains/kotlin/cli/jvm/compiler/KotlinCoreEnvironment.kt"
                                                      :msg    "(31, 26): Unresolved reference: jvm"}
                                                     {:commit {:hash    "f05e9266705b7dfc64cedd5eedc66605e6ddcb64"
                                                               :mail    "nikolay.krasko@jetbrains.com"
                                                               :message "Update idea to 173.3.4 (173.4548.28)"
                                                               :time    (Instant/ofEpochSecond 1517606145)}
                                                      :file   "compiler/cli/src/org/jetbrains/kotlin/cli/AnotherFile.kt"
                                                      :msg    "anothererror"}]
                                       :finish-date #inst "2018-02-02T21:15:45.000-00:00"
                                       :bunch  "master_as30"
                                       :branch    "master"}
                                      {:branch "1.2.50"
                                       :bunch  "1.2.50_181"})
                                (collect-branch-info {"master" {"master_172"  "Kotlin_dev_CompilerAndPlugin_172"
                                                                "master_as30" "Kotlin_dev_CompilerAndPlugin_as30"}
                                                      "1.2.50" {"1.2.50_181"  "Kotlin_dev_CompilerAndPlugin_1"}})))))))

(deftest get-updated-branches-test
  (testing "get-updated-branches-test"
    (expect-proc-calls [{:args ["git" "fetch" "--all" "-p"]
                         :out  "Fetching origin"
                         :err  (str/join "\n" '("From github.com:JetBrains/kotlin"
                                                    "   228aeee4ede..fa0f56dfd17  master_181  -> origin/master_181"
                                                    " + eb004eb...4bc5107 1.2.30_172 -> origin/1.2.30_172  (forced update)"
                                                    " * [new branch]              mitropo/tmp -> origin/mitropo/tmp"))
                         :code 0}]
                       (is (= (list "master_181" "1.2.30_172" "mitropo/tmp") (fetch))))))


(deftest test-has-coflicts
  (testing "test-has-coflicts" (is (not (has-conflicts
                                          [{:bunch "master_181", :branch "master", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "master_171", :branch "master", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "master_172", :branch "master", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "master_as30", :branch "master", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "master_as31", :branch "master", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "master_as32", :branch "master", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "1.2.30_181", :branch "1.2.30", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "1.2.30_172", :branch "1.2.30", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "1.2.30_171", :branch "1.2.30", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "1.2.30_as30", :branch "1.2.30", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "1.2.30_as31", :branch "1.2.30", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}
                                           {:bunch "1.2.30_as32", :branch "1.2.30", :finish-date (Instant/ofEpochSecond 1517606145), :conflicts (list)}])))))

(deftest get-new-conflicts-test
  (testing "get-new-conflicts"
    (is (= '() (get-new-conflicts
                 [{:conflicts nil :bunch "master_173" :branch "master"}
                  {:conflicts nil :bunch "1.2.30_181" :branch "1.2.30"}
                  {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
                 [{:conflicts nil :bunch "master_173" :branch "master"}
                  {:conflicts nil :bunch "1.2.30_181" :branch "1.2.30"}
                  {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
                 )))
    (is (= [] (get-new-conflicts
                [{:conflicts nil :bunch "master_173" :branch "master"}
                 {:conflicts [{:commit {:mail "nikolay.krasko@jetbrains.com"
                                   :message "Update idea to 173.3.4 (173.4548.28)"
                                   :time    (Instant/ofEpochSecond 1517606145)}
                          :file   "versions.gradle.kts"}]
                  :bunch     "1.2.30_181" :branch "1.2.30"}
                 {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
                [{:conflicts [{:commit {:mail "nikolay.krasko@jetbrains.com"
                                   :message "Update idea to 173.3.4 (173.4548.28)"
                                   :time    (Instant/ofEpochSecond 1517606145)}
                          :file   "versions.gradle.kts"}]
                  :bunch     "master_173" :branch "master"}
                 {:conflicts [{:commit {:mail "nikolay.krasko@jetbrains.com"
                                   :message "Update idea to 173.3.4 (173.4548.28)"
                                   :time    (Instant/ofEpochSecond 1517606145)}
                          :file   "versions.gradle.kts"}]
                  :bunch     "1.2.30_181" :branch "1.2.30"}
                 {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}])))
    (is (= (list {:branch    "master"
             :bunch     "master_173"
             :conflicts [{:commit {:mail    "nikolay.krasko@jetbrains.com"
                                   :message "Update idea to 173.3.4 (173.4548.28)"
                                   :time    (Instant/ofEpochSecond 1517606145)}
                          :file   "versions.gradle.kts"}
                         {:commit {:mail    "nikolay.krasko@jetbrains.com"
                                   :message "another commit"
                                   :time    (Instant/ofEpochSecond 1517607145)}
                          :file   "another file"}]})
           (get-new-conflicts
            [{:conflicts  [{:commit {:mail    "nikolay.krasko@jetbrains.com"
                                     :message "Update idea to 173.3.4 (173.4548.28)"
                                     :time    (Instant/ofEpochSecond 1517606145)}
                            :file   "versions.gradle.kts"}
                           {:commit {:mail    "nikolay.krasko@jetbrains.com"
                                     :message "another commit"
                                     :time    (Instant/ofEpochSecond 1517607145)}
                            :file   "another file"}]
              :bunch "master_173" :branch "master"}
             {:conflicts nil :bunch "1.2.30_181" :branch "1.2.30"}
             {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
            [{:conflicts [{:commit {:mail    "nikolay.krasko@jetbrains.com"
                                    :message "Update idea to 173.3.4 (173.4548.28)"
                                    :time    (Instant/ofEpochSecond 1517606145)}
                           :file   "versions.gradle.kts"}] :bunch "master_173" :branch "master"}
             {:conflicts nil :bunch "1.2.30_181" :branch "1.2.30"}
             {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}])))))



(deftest notifications
  (testing "states-sample"
    (let [states [[{:conflicts nil :bunch "master_173" :branch "master"}
                   {:conflicts nil :bunch "1.2.30_181" :branch "1.2.30"}
                   {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
                  [{:conflicts nil :bunch "master_173" :branch "master"}
                   {:conflicts nil :bunch "1.2.30_181" :branch "1.2.30"}
                   {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
                  [{:conflicts nil :bunch "master_173" :branch "master"}
                   {:conflicts  [{:commit {:mail    "nikolay.krasko@jetbrains.com"
                                           :message "Update idea to 173.3.4 (173.4548.28)"
                                           :time    (Instant/ofEpochSecond 1517606145)}
                                  :file   "versions.gradle.kts"}]
                    :bunch "1.2.30_181" :branch "1.2.30"}
                   {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
                  [{:conflicts nil :bunch "master_173" :branch "master"}
                   {:conflicts  [{:commit {:mail    "nikolay.krasko@jetbrains.com"
                                           :message "Update idea to 173.3.4 (173.4548.28)"
                                           :time    (Instant/ofEpochSecond 1517606145)}
                                  :file   "versions.gradle.kts"}]
                    :bunch "1.2.30_181" :branch "1.2.30"}
                   {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
                  [{:conflicts [{:commit {:mail    "master_173-conflicter@jetbrains.com"
                                          :message "new conflict in master_173"
                                          :time    (Instant/ofEpochSecond 1517606145)}
                                 :file   "versions.gradle.kts"}]
                    :bunch "master_173" :branch "master"}
                   {:conflicts  [{:commit {:mail    "nikolay.krasko@jetbrains.com"
                                           :message "Update idea to 173.3.4 (173.4548.28)"
                                           :time    (Instant/ofEpochSecond 1517606145)}
                                  :file   "versions.gradle.kts"}]
                    :bunch "1.2.30_181" :branch "1.2.30"}
                   {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
                  [{:conflicts  [{:commit {:mail    "nikolay.krasko@jetbrains.com"
                                           :message "Update idea to 173.3.4 (173.4548.28)"
                                           :time    (Instant/ofEpochSecond 1517606145)}
                                  :file   "versions.gradle.kts"}
                                 {:commit {:mail    "nikolay.krasko@jetbrains.com"
                                           :message "another commit"
                                           :time    (Instant/ofEpochSecond 1517607145)}
                                  :file   "another file"}]
                    :bunch "master_173" :branch "master"}
                   {:conflicts nil :bunch "1.2.30_181" :branch "1.2.30"}
                   {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
                  [{:conflicts nil :bunch "master_173" :branch "master"}
                   {:conflicts nil :bunch "1.2.30_181" :branch "1.2.30"}
                   {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]
                  [{:conflicts nil :bunch "master_173" :branch "master"}
                   {:conflicts nil :bunch "1.2.30_181" :branch "1.2.30"}
                   {:conflicts nil :bunch "1.2.30_171" :branch "1.2.30"}]]
          notifications (slice-reduce process-status-update nil states)]
      (is (= [nil
              nil
              [{:conflicts  [{:commit {:mail    "nikolay.krasko@jetbrains.com"
                                       :message "Update idea to 173.3.4 (173.4548.28)"
                                       :time    (Instant/ofEpochSecond 1517606145)}
                              :file   "versions.gradle.kts"}]
                :bunch "1.2.30_181"
                :branch   "1.2.30"}]
              nil
              [{:conflicts [{:commit {:mail    "master_173-conflicter@jetbrains.com"
                                      :message "new conflict in master_173"
                                      :time    (Instant/ofEpochSecond 1517606145)}
                             :file   "versions.gradle.kts"}]
                :bunch "master_173" :branch "master"}]
              [{:conflicts  [{:commit {:mail    "nikolay.krasko@jetbrains.com"
                                       :message "Update idea to 173.3.4 (173.4548.28)"
                                       :time    (Instant/ofEpochSecond 1517606145)}
                              :file   "versions.gradle.kts"}
                             {:commit {:mail    "nikolay.krasko@jetbrains.com"
                                       :message "another commit"
                                       :time    (Instant/ofEpochSecond 1517607145)}
                              :file   "another file"}]
                :bunch "master_173"
                :branch   "master"}]
              [{:branch    "master"
                :bunch     "master_173"
                :conflicts nil}
               {:branch    "1.2.30"
                :bunch     "1.2.30_181"
                :conflicts nil}
               {:branch    "1.2.30"
                :bunch     "1.2.30_171"
                :conflicts nil}]
              nil] (vec notifications))))))

(deftest notification-detector-test
  (testing "notification-detector-latest-build"
    (with-redefs [kt-merge-bot.teamcity/latest-build-for-configuration
                  (sequential-mock [{:args ["master" "Kotlin_dev_CompilerAndPlugin_171"] :out "1"}
                                      {:args ["1.2.30" "Kotlin_1230_CompilerAndPlugin_181"]}
                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_181"] :out "2"}

                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_171"] :fail true}
                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_171"] :out "1"}
                                      {:args ["1.2.30" "Kotlin_1230_CompilerAndPlugin_181"]}
                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_181"] :out "2"}

                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_171"] :out "1"}
                                      {:args ["1.2.30" "Kotlin_1230_CompilerAndPlugin_181"] :out "3"}
                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_181"] :out "2"}

                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_171"] :out "1"}
                                      {:args ["1.2.30" "Kotlin_1230_CompilerAndPlugin_181"] :out "3"}
                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_181"] :out "4"}

                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_171"] :out "5"}
                                      {:args ["1.2.30" "Kotlin_1230_CompilerAndPlugin_181"] :out "3"}
                                      {:args ["master" "Kotlin_dev_CompilerAndPlugin_181"] :out "4"}
                                      ]
                                   (fn [line]
                                       (when (:fail line) (throw (TeamCityConversationException. "test error" nil)))
                                       (when-let [id (:out line)]
                                         (reify org.jetbrains.teamcity.rest.Build
                                           (fetchFinishDate [this] (Date/from (Instant/ofEpochSecond 1517606145)))
                                           (getId [this] (BuildId. id))
                                           (getWebUrl [this] "http://build.url")))))]
      (let [has-new-build (new-build-detector {"master" {"master_181" "Kotlin_dev_CompilerAndPlugin_181"
                                                         "master_171" "Kotlin_dev_CompilerAndPlugin_171"}
                                               "1.2.30" {"1.2.30_181" "Kotlin_1230_CompilerAndPlugin_181"}})]
        (is (= [true false true true true] (repeatedly 5 has-new-build)))))))
