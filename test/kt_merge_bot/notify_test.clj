(ns kt-merge-bot.notify-test
  (:require [clojure.test :refer :all]
            [kt-merge-bot.parser :refer :all]
            [kt-merge-bot.git-info :refer :all]
            [kt-merge-bot.test-utils :refer :all]
            [kt-merge-bot.utils :refer :all]
            [kt-merge-bot.core :refer :all]
            [kt-merge-bot.slack :as slk]
            [clojure.string :as str])
  (:import (java.time Instant)
           (java.util Date)
           (org.jetbrains.teamcity.rest BuildId TeamCityConversationException)))


(deftest test-update-notifications
  (testing "parsing-merge-result"
    (let [mock-slack
          (sequential-mock
            [{:args [:post nil nil [{:conflict "CONFLICT_1", :in ["1.2.30_181" "1.2.30_182" "1.2.30_183"]}]] :result 2}
             {:args [:update nil 2 [{:conflict "CONFLICT_1", :in ["1.2.30_181" "1.2.30_182" "1.2.30_183" "1.2.30_as32"]}]] :result 2}
             {:args [:post nil nil [{:conflict [{:commit "new conflict in master_173", :file "versions.gradle.kts"}], :in ["master_173"]}]] :result 5}
             {:args [:post nil nil [{:conflict [{:commit "Update idea to 173.3.4 (173.4548.28)", :file "versions.gradle.kts"} {:commit "another commit", :file "another file"}], :in ["master_173"]}]] :result 6}
             {:args [:post nil nil [{:conflict nil, :in ["master_173" "1.2.30_181" "1.2.30_171"]}]] :result 7}])]

      (with-redefs [slk/post-message (fn [ch text] (mock-slack :post ch nil text))
                    slk/upd-message (fn [ch ts text] (mock-slack :update ch ts text))]
        (let [CONFLICT_1 [{:commit {:mail    "nikolay.krasko@jetbrains.com"
                                    :message "Update idea to 173.3.4 (173.4548.28)"
                                    :time    (Instant/ofEpochSecond 1517606145)}
                           :file   "versions.gradle.kts"}]
              test-render (fn [arg] (->> arg
                                         (group-by :conflicts)
                                         (mapv (fn [[conflicts items]]
                                                 {:conflict
                                                      (condp = conflicts
                                                        CONFLICT_1 "CONFLICT_1"
                                                        conflicts)
                                                  :in (->> items (mapv :bunch))}))))
              post-updater-mock (slk/post-updater nil concat test-render)
              ntf [nil
                   nil
                   [{:conflicts CONFLICT_1
                     :bunch     "1.2.30_181"
                     :branch    "1.2.30"}
                    {:conflicts CONFLICT_1
                     :bunch     "1.2.30_182"
                     :branch    "1.2.30"}
                    {:conflicts CONFLICT_1
                     :bunch     "1.2.30_183"
                     :branch    "1.2.30"}
                    {:conflicts nil
                     :bunch     "1.2.30_as32"
                     :branch    "1.2.30"}
                    ]
                   [{:conflicts CONFLICT_1
                     :bunch     "1.2.30_as32"
                     :branch    "1.2.30"}
                    ]
                   nil
                   [{:conflicts [{:commit "new conflict in master_173"
                                  :file   "versions.gradle.kts"}]
                     :bunch     "master_173" :branch "master"}]
                   [{:conflicts [{:commit "Update idea to 173.3.4 (173.4548.28)"
                                  :file   "versions.gradle.kts"}
                                 {:commit "another commit"
                                  :file   "another file"}]
                     :bunch     "master_173"
                     :branch    "master"}]
                   [{:branch    "master"
                     :bunch     "master_173"
                     :conflicts nil}
                    {:branch    "1.2.30"
                     :bunch     "1.2.30_181"
                     :conflicts nil}
                    {:branch    "1.2.30"
                     :bunch     "1.2.30_171"
                     :conflicts nil}]
                   nil]]
          (doseq [n ntf]
            (-notify-grouped post-updater-mock n))
          )))))










