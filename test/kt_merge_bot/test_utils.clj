(ns kt-merge-bot.test-utils
  (:require [clojure.test :refer :all])
  (:import (java.io StringReader)))

(defn mock-proc [{:keys [err out code]}]
  {:out     (StringReader. (or out ""))
   :err     (StringReader. (or err ""))
   :process (proxy [Process] [] (waitFor [] (or code 0)))})

(defn sequential-mock
  ([commands] (sequential-mock commands (fn [line] (:result line))))
  ([commands handler] (sequential-mock commands handler identity))
  ([commands handler pre-handler]
   (let [index (atom 0)
         next-line (fn [] (get commands (-> index (swap! inc) (dec))))]
     (fn [& args]
       (let [cmd (pre-handler args)
             line (next-line)]
         (if (= (:args line) cmd)
           (handler line)
           (throw (AssertionError. (let [expected (:args line)]
                                     (if-not (nil? expected)
                                       (str "expected command " expected " but got " cmd)
                                       (str "no command expected but got " cmd)))))))))))

(defn non-key-args [args] (first (map vec (split-with (complement keyword?) args))))

(defmacro expect-proc-calls [commands & body]
  `(with-redefs [me.raynes.conch.low-level/proc (sequential-mock ~commands ~mock-proc non-key-args)] ~@body))
