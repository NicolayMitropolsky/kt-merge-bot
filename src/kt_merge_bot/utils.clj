(ns kt-merge-bot.utils
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn wrap [prefix suffix value] (str prefix value suffix))

(defn strip-suffix [^String suffix ^String s ]
  (if (str/ends-with? s suffix)
    (subs s 0 (- (count s) (count suffix))) s))

(defn slice-reduce [f state coll]
  (when-first [h coll]
    (let [[ns r] (f state h)]
      (cons r (slice-reduce f ns (rest coll))))))

(defn match-by [criteria new-records old-records]
  (map (fn [new] [new (first (filter #(= (criteria new) (criteria %)) old-records))]) new-records))

(defprotocol StateMachine
  (accept [this message])
  (state [this]))

(defn make-state-machine [handler init]
  (let [state (atom init)]
    (reify StateMachine
      (accept [this message]
        (let [[new_state r] (handler @state message)]
          (reset! state new_state) r))
      (state [this] (deref state)))))

(defn take-if [pred val]
  (when (pred val) val))

(defn also [pred val]
  (pred val) val)

(defn file-lines [name]
  (some->> (io/file name)
           (take-if #(.exists %))
           (slurp)
           (str/split-lines)))

(defmacro retry [{:keys [catcher repeats timeout not-throw?]} & handler]
  `(loop [i# (or ~repeats 0)]
    (let [result# (try (do ~@handler)
                       (catch Exception e#
                         (if (~catcher e#)
                           (if (<= i# 0)
                             (if-not ~not-throw? (throw e#) nil)
                             ::failure#)
                           (throw e#))))]
      (if (identical? ::failure# result#)
        (do (Thread/sleep (or ~timeout 0))
            (recur (dec i#))) result#))))