(ns kt-merge-bot.teamcity
  (:require [clj-http.client :as client]
            [clojure.string :as str]
            [kt-merge-bot.utils :as u]
            [instaparse.core :as insta]
            [clojure.tools.logging :as log])
  (:import (java.io InputStreamReader BufferedReader IOException)
           (java.util.zip ZipInputStream)
           (org.jetbrains.teamcity.rest TeamCityInstanceFactory BuildConfigurationId Build TeamCityRestException)
           (com.google.common.cache CacheBuilder CacheLoader LoadingCache)))

(def ^:dynamic teamcity-log-url "https://teamcity.jetbrains.com/guestAuth/downloadBuildLog.html")

(defn get-build-log-errors [build-num-str lines-handler]
  (with-open [body (:body (client/get teamcity-log-url {:query-params {:buildId build-num-str :archived true} :as :stream}))
              zip (ZipInputStream. body)]
    (.getNextEntry zip)
    (->> (InputStreamReader. zip) (BufferedReader.) (line-seq) (lines-handler))))

(def parser (insta/parser
              "S = ktErr | javaErr
               <ktErr> = time <'E:'> whitespace module errSep fullfilepath <':'> whitespace msg
               <javaErr> = time <'E:'> whitespace module whitespace fullfilepath linenum <':'> whitespace <'error:'> msg
               time = #'\\[\\d\\d:\\d\\d:\\d\\d\\]'
               <errSep> = <#'\\s*([Ee]:)\\s+'>
               module = #'\\[\\S+\\]'
               <fullfilepath> = <filesep? filename ':'? (filesep filename)* filesep 'work' filesep filename filesep filename filesep> filepath
               linenum = #':\\d+'
               filepath = filename (<filesep> filename)*
               <filename> = #'[\\w\\d\\.-]+'
               <filesep> = '\\\\' | '/'
               <whitespace> = <#'\\s+'>
               msg = #'.*'"))


(defn collect-compile-errors [lines]
  (->> lines
       (keep (fn [line] (let [result (parser line)]
                          (when-not (insta/failure? result)
                            (select-keys
                              (insta/transform {:S        (fn [& els] (into {} els))
                                                :filepath (fn [& args] {:file (str/join "/" args)})} result)
                              [:file :msg])))))
       (vec)))


(defn -get-compilation-errors [build-num-str]
  (log/info "getting get-compilation-errors (uncached) for" build-num-str)
  (u/retry {:catcher (fn [e]
                       (log/error "error in get-compilation-errors " build-num-str (.getMessage e))
                       (.printStackTrace e)
                       (some #(instance? % e) [IOException TeamCityRestException]))
            :repeats 3 :timeout 2000 :not-throw? false}
           (get-build-log-errors build-num-str collect-compile-errors)))

(def -compile-errors-cache (-> (CacheBuilder/newBuilder)
                                             (.maximumSize 30)
                                             (.build (proxy [CacheLoader] []
                                                       (load [build-num-str]
                                                         (-get-compilation-errors build-num-str))))))

(defn get-compilation-errors [build-num-str]
  (log/info "getting get-compilation-errors (cached) for " build-num-str)
  (.get -compile-errors-cache build-num-str))

(defn latest-build-for-configuration ^Build [branch conf-name]
  (log/info "getting latest for " branch  "conf" conf-name)
  (-> (TeamCityInstanceFactory/guestAuth "https://teamcity.jetbrains.com") (.builds) (.fromConfiguration (BuildConfigurationId. conf-name))
      (.withAnyStatus) (.withBranch branch) (.latest)))

(defn latest-build-for-configuration-with-retry ^Build [branch conf-name]
  (u/retry {:catcher (fn [e] (log/error "error in latest-build-for-configuration " (.getMessage e)) (.printStackTrace e) (instance? TeamCityRestException e))
            :repeats 3 :timeout 2000 :not-throw? true}
           (latest-build-for-configuration branch conf-name)))