(ns kt-merge-bot.git-info
  (:require [me.raynes.conch :refer [programs execute]]
            [kt-merge-bot.parser :as p]
            [clojure.string :as str]
            [clojure.tools.logging :as log]))

(def ^:dynamic *repo_path* "<path-should-be-set-up-on-load>")

(defn execute-in-repo [& args-all]
  (let [args (filter some? args-all)
        end (last args)
        args (if (map? end)
               (-> args (drop-last) (vec) (conj (assoc end :dir *repo_path*)))
               (-> args (vec) (conj {:dir *repo_path*})))]
    (log/debug "executing = " args)
    (apply execute args)))

(defn current-branch []
  (str/trim (execute-in-repo "git" "symbolic-ref" "--short" "HEAD")))

(defn revert-merge []
  (log/info "reverting merge")
  (execute-in-repo "git" "reset" "--merge"))

(defn log [file commits-count opts]
  (log/debug "log" file  "opt = " opts)
  (->> (execute-in-repo "git" "log" (when (some #{:merge} opts) "--merge") "-n" commits-count "--format=%H %ct %ae %ce %s" "--" file {:seq true})
       (vec)))

(defn cherry [base branch]
  (->> (execute-in-repo "git" "cherry" "-v" (str "origin/" base ) (str "origin/" branch) {:seq true})
       (map (fn [line] (let [els (str/split line #" " 3)] (when (-> els (count) (> 2)) (get els 2)))))))

(defn fetch []
  (let [value (execute-in-repo "git" "fetch" "--all" "-p" {:verbose true :seq true})
        fetched-updates (->> value :proc :err (keep #(->> % (re-find #"origin/(\S+)(?:  \(forced update\))?$") (last))))]
    (log/debug "fetched-updates = " fetched-updates " inc values err = " (->> value :proc :err) )
    fetched-updates))

(defn blame [file & opts]
  (log/spyf (str "blame for file: " file " is %s")
            (->> (log file 1 opts) (map p/commit-info) (first))))



