(ns kt-merge-bot.slack
  (:require
    [clojure.string :as str]
    [clj-http.client :as client]
    [clojure.java.io :as io]
    [kt-merge-bot.utils :as u]
    [clojure.tools.logging :as log]
    )
  (:import (com.google.common.cache CacheBuilder)
           (java.util.concurrent TimeUnit)))

(def ^:dynamic *token* "<slack-token-should-be-specified>")

(defn post-message [channel text]
  (log/info "posting " "channel = " channel " text = " text)
  (let [result (client/post "https://slack.com/api/chat.postMessage"
                            {:form-params {:token *token* :channel channel :text text :link_names 1}
                             :as          :json})]
    (log/debug "post result =" result)
    (some->> result :body :ts)))

(defn upd-message [channel ts text]
  (log/info "updating " "channel = " channel " ts = " ts " text = " text)
  (let [result (client/post "https://slack.com/api/chat.update"
                            {:form-params {:token *token* :channel channel :text text :ts ts :link_names 1}
                             :as          :json})]
    (log/debug "upd result =" result)
    (some->> result :body :ts)))


(defn post-updater [channel merger-fn render-fn]
  (let [ts-cache (-> (CacheBuilder/newBuilder)
                     (.maximumSize 10)
                     (.expireAfterAccess 4 TimeUnit/HOURS)
                     (.build))]
    (fn [key info]
      (log/debug "post-updater key = " key " info = " info)
      (if key
        (let [[ts old-info] (.getIfPresent ts-cache key)
              _ (log/debug "ts for " (System/identityHashCode ts-cache) key " = " ts)
              new-info (if old-info (merger-fn old-info info) info)
              text (render-fn new-info)
              ts (if (nil? ts)
                   (post-message channel text)
                   (upd-message channel ts text))]
          (log/debug (System/identityHashCode ts-cache) "saving" key [ts new-info])
          (.put ts-cache key [ts new-info]))
        (do
          (.invalidateAll ts-cache)
          (post-message channel (render-fn info)))))))


