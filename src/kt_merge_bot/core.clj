(ns kt-merge-bot.core
  (:require [clojure.edn :as edn]
            [kt-merge-bot.git-info :as g]
            [kt-merge-bot.teamcity :as t]
            [kt-merge-bot.utils :refer :all]
            [kt-merge-bot.slack :as slack]
            [clojure.string :as str]
            [kt-merge-bot.utils :as u]
            [org.httpkit.server :as httpkit]
            [clojure.tools.logging :as log])
  (:gen-class)
  (:import (org.ocpsoft.prettytime PrettyTime)
           (java.util Date)
           (org.jetbrains.teamcity.rest Build BuildId TeamCityRestException)
           (java.time Instant Duration)))


(defn -branches [{:keys [branch bunch]}]
  {:branch branch :bunch bunch})

(defn -conflicts-includes [new-conflicts old-conflicts]
  (if new-conflicts
    (->> (match-by :file new-conflicts old-conflicts) (not-any? (fn [[new old]] (nil? old))))
    true))

(defn get-new-conflicts [new-records old-records]
  (->> (match-by -branches new-records old-records)
       (filter (fn [[new old]] (and (:conflicts new) (not (-conflicts-includes (:conflicts new) (:conflicts old))))))
       (map first)))

(defn format-date [date]
  (condp instance? date
    Instant (format-date (Date/from date))
    Date (.format (PrettyTime.) ^Date date)))

(def ^:dynamic *interval-to-mention* 600)

(defn get-slack-name [email]
  (when-let [line (some->> (file-lines "names.txt")
                           (filter (fn [line] (str/starts-with? line (str/lower-case email))))
                           (first))]
    (last (str/split line #"\s+"))))

(defn commit-line [file should-mention? commit]
  (let [as-name (fn [email] (or (when (and should-mention? (-> (:time commit) (Duration/between (Instant/now)) (.getSeconds) (< *interval-to-mention*)))
                                  (get-slack-name email))
                                (strip-suffix "@jetbrains.com" email)))
        committer (or (some->> commit (:committer-mail) (as-name) (wrap "/" "")) "")]
    (str "*" (as-name (:mail commit)) committer "*" " <" "https://github.com/JetBrains/kotlin/blob/" (:hash commit) "/" file "|" (format-date (:time commit)) ">")))

(defn build-ref [finish-date build-url]
  (when (and finish-date build-url)
    (str "<" build-url "|" (format-date finish-date) ">")))

(defn pretty-print [build-configurations builds-infos]
  (let [conflicts (:conflicts (first builds-infos))]
    (str (->> builds-infos
              (map (fn [{:keys [branch bunch finish-date build-url]}]
                     (let [bb (str branch "." bunch)
                           branch (if-some [configuration (get-in build-configurations [branch bunch])]
                                    (str "<" "https://teamcity.jetbrains.com/viewType.html?buildTypeId=" configuration "&branch_Kotlin_dev=%3Cdefault%3E&tab=buildTypeStatusDiv"
                                         "|" bb ">") bb)]
                       (if (empty? conflicts)
                         (str ":white_check_mark: *" branch "* :OK" "\n")
                         (str ":no_entry:" " *" branch "* :FAIL" (some->> (build-ref finish-date build-url) (wrap " (" ")")) "\n")))))
              (str/join))
         (when (seq conflicts)
           (str (->> conflicts
                     (take 5)
                     (map #(str "    `" (:file %) "` : _" (:msg %) "_" (some->> % :commit (commit-line (:file %) false) (wrap " (" ")"))))
                     (str/join "\n"))
                (if (> (count conflicts) 5) (str "\n    + " (- (count conflicts) 5) " more problems") nil))))))


(defn has-conflicts [merge-results]
  (some #(not-empty (:conflicts %)) merge-results))


(defn get-compile-errors-from-latest-build [main-branch build-configuration]
  (u/retry {:catcher    (fn [e] (log/error "error in get-compile-errors-from-latest-build " (.getMessage e)) (.printStackTrace e) (instance? TeamCityRestException e))
            :repeats    3
            :not-throw? true}
           (when-let [^Build latest-build (some->> build-configuration
                                           (t/latest-build-for-configuration main-branch))]

             {:finish-date (.fetchFinishDate latest-build)
              :build-url   (.getWebUrl latest-build)
              :conflicts   (vec (some->> latest-build
                                     (take-if (fn [build] (let [statusText (.fetchStatusText build)]
                                                            (log/debug "get-compile-errors-from-latest-build text = " statusText)
                                                            (.contains (.toLowerCase statusText) "compile"))))
                                     (.getId) (.getStringId)
                                     (t/get-compilation-errors) (map #(assoc % :commit (->> (:file %) (g/blame)))) (vec)))})))

(defn collect-branch-info [build-configurations]
  (->> build-configurations
       (mapcat
         (fn [[main-branch configurations-mapping]]
           (g/execute-in-repo "git" "checkout" "-f" (str "origin/" main-branch))
           (->> configurations-mapping
                (map (fn [[bunch configuration]]
                       (merge
                         {:bunch bunch
                          :branch main-branch}
                         (some->> configuration (get-compile-errors-from-latest-build main-branch))) )) (vec))))
       (vec)))

(defn new-build-detector [build-configurations]
  (let [state (make-state-machine (fn [old new]
                                    (log/info "new-build-detector old = " old)
                                    (log/info "new-build-detector new = " new)
                                    [new (not= old new)]) nil)
        master-branch-mapping (->> build-configurations
                                   (mapcat
                                     (fn [[main-branch configurations]]
                                       (->> configurations (vals) (mapcat (fn [conf] [conf main-branch])))))
                                   (apply hash-map))]
    (fn []
      (->> master-branch-mapping
           (mapcat (fn [[conf-name branch]]
                     [conf-name (some->> (some-> branch (t/latest-build-for-configuration-with-retry conf-name)) (.getId) (.getStringId))]))
           (apply hash-map)
           (accept state)))))

(defn process-status-update [old-status merge-status-list]
  (log/info "merge-status-list = " merge-status-list)
  (let [[new_status to_notify]
        (let [conflicts-found (has-conflicts merge-status-list)]
          (log/info "conflicts-found = " conflicts-found)
          [(if conflicts-found
             {:state merge-status-list :time (System/currentTimeMillis)}
             nil)
           (cond
             conflicts-found (not-empty (get-new-conflicts merge-status-list (:state old-status)))
             (and (not conflicts-found) (has-conflicts (:state old-status))) merge-status-list
             )])]
    (log/info "old-status = " old-status)
    (log/info "new_status = " new_status)
    (log/info "to-notify = " to_notify)
    [new_status to_notify]))

(defn notification-loop [notify {:keys [interval build-configurations]}]
  (let [problems-state (make-state-machine process-status-update nil)
        has-new-build (new-build-detector build-configurations)]
    (loop [first-time true]
      (let [start_time (System/currentTimeMillis)]

        (when (or first-time
                  (has-new-build))
          (u/retry {:catcher (fn [e] (= "Program returned non-zero exit code 1" (.getMessage e)))
                    :repeats 5
                    :timeout 20000}
                   (g/fetch))
          (some->> (collect-branch-info build-configurations) (accept problems-state) (notify)))

        (let [pause (max (-> (* interval 1000) (- (-> (System/currentTimeMillis) (- start_time)))) 0)]
          (log/info "pause = " pause)
          (Thread/sleep pause))
        (recur false)))))

(defn -notify-grouped [notify-in-slack merge-status-list]
  (if (some #(not-empty (:conflicts %)) merge-status-list)
    (doseq [[conflict members] (group-by :conflicts merge-status-list)]
      (when (not-empty conflict)
        (notify-in-slack conflict members)))
    (when (not-empty merge-status-list)
      (notify-in-slack nil merge-status-list))))

(defn -main []
  (let [config (edn/read-string (slurp "config.edn"))
        {:keys [repo interval slack-token out-channel err-channel build-configurations web-port]} config
        web-server-stopper (httpkit/run-server (fn [req]
                                         {:status  200
                                          :headers {"Content-Type" "text/plain"}
                                          :body    "OK"}) {:port (or web-port 12080)})
        post-updater (slack/post-updater out-channel concat (partial pretty-print build-configurations))
        notify (fn [merge-status-list] (-notify-grouped post-updater merge-status-list))]
    (binding [g/*repo_path* repo
              slack/*token* slack-token
              *interval-to-mention* (* 2 interval)]
      (try
        (notification-loop notify config)
        (catch Throwable e
          (.printStackTrace e)
          (u/retry {:catcher (fn [_] true) :repeats 3 :timeout 5000} (slack/post-message err-channel
                                                                                         (str (.getMessage e)
                               "\n```"
                               (with-out-str (clojure.stacktrace/print-stack-trace e))
                               "\n```"))))
        (finally (web-server-stopper))))))