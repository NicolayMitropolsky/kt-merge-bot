(ns kt-merge-bot.parser
  (:require [clojure.string :as str])
  (:import (java.time Instant)))

(defn get-conflict-files [merge-lines]
  (->> merge-lines
       (filter #(str/starts-with? % "CONFLICT"))
       (map #(last (or
                     (re-find #"CONFLICT \(content\): Merge conflict in (.*)" %)
                     (re-find #"CONFLICT \(modify/delete\): (.*) deleted" %))))))

(defn commit-info [line]
  (let [words (str/split line #" ")]
    (if (< (count words) 2)
      nil
      (-> {:hash    (get words 0)
           :time    (-> words (get 1) (Long/parseLong) (Instant/ofEpochSecond))
           :mail    (get words 2)
           :message (->> words (drop 4) (str/join " "))}
          (cond->
            (not= (get words 2) (get words 3)) (assoc :committer-mail (get words 3)))))))