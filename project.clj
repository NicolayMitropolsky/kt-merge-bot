(defproject kt-merge-bot "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :repositories [["bintray" {:url "https://dl.bintray.com/jetbrains/teamcity-rest-client"}]]
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [me.raynes/conch "0.8.0"]
                 [clj-http "3.7.0"]
                 [cheshire "5.8.0"]
                 [org.jetbrains.teamcity/teamcity-rest-client "0.1.95"]
                 [com.google.guava/guava "24.0-jre"]
                 [org.ocpsoft.prettytime/prettytime "3.2.7.Final"]
                 [instaparse "1.4.8"]
                 [http-kit "2.3.0"]
                 [org.clojure/tools.logging "0.4.1"]
                 [org.apache.logging.log4j/log4j-api "2.12.0"]
                 [org.apache.logging.log4j/log4j-core "2.12.0"]
                 [org.apache.logging.log4j/log4j-slf4j-impl "2.12.0"]]
  :aot [kt-merge-bot.core]
  :main kt-merge-bot.core
  )
